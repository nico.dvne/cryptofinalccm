import hashlib
from reduction import Rainbow
import createTable

def cleanHashFromFile(dirtyHash):
    toCleanHash = list(dirtyHash)
    if toCleanHash[-1] == "\n":
        toCleanHash.pop()

    return ''.join(toCleanHash)

def checkIfHashInTable(researchedHash):
    with open('rainbow_table.txt', 'r') as rainbowTableFile:
        for line in rainbowTableFile.readlines():
            lineHash = cleanHashFromFile(line.split('|')[1])

            if researchedHash == lineHash : 
                print(researchedHash)
                return True
        
        return False

def getFirstColumnWord(givenHash):
    with open('rainbow_table.txt', 'r') as rainbowTableFile:
        for line in rainbowTableFile.readlines():
            splitFile = line.split('|')
            theWord = splitFile[0]
            lineHash = cleanHashFromFile(splitFile[1])

            if lineHash == givenHash:
                return theWord

        return False

def crackWord(givenHash):
    for i in range(0, createTable.REPETITION_M + 2):
        if checkIfHashInTable(givenHash):
            #the first word -> to hash
            password = getFirstColumnWord(givenHash)
            for i in range(0, createTable.REPETITION_M + 2):
                hash = createTable.passwordToHash(password)
                if hash == givenHash:
                    return password
                
                password = createTable.hashToPassword(hash)

        else:
            givenHash = createTable.iterate(givenHash)

    return "Pas de hash trouvé"



givenHash = input('Saisir le hash a rechercher :')
print(crackWord(givenHash))
