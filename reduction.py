import base64

class Rainbow:
 
    @staticmethod
    def reduct(hash):
        reduced = ""
        number_letter_reduced = 0
        number_digit_reduced = 0
        for character in reversed(str(hash)): # Loop over the reversed hash
            try:
                int(character)
                if(number_digit_reduced < 4):
                    reduced += character
                    number_digit_reduced += 1
            except ValueError: # it was a string, not an int.
                if(character.isalpha() and number_letter_reduced < 6):
                    reduced += character.lower()
                    number_letter_reduced += 1

        return reduced
