import random
import argparse
import hashlib
from reduction import Rainbow

REPETITION_M = 1000
POSSIBILITIES = 100

def insertNumberInWord(word):
    wordList = list(word.strip(" "))
    if wordList[-1] == "\n":
        wordList.pop()
    for i in range(0,4):
        position = random.randint(0, len(wordList))
        numero = random.randint(0, 9)
        wordList.insert(position, str(numero))

    return ''.join(wordList)


def getHashFromWord(word):
    return str(hashlib.md5(word.encode()).hexdigest())


def getFileWords():
    words = []
    with open ('words_ccm.txt', 'r') as file :
        for line in file:
            words.append(line)

    return words

def getRainbowPasswords():
    rainbowPasswords = []
    for word in getFileWords():
        for i in range(0, POSSIBILITIES):
            rainbowPasswords.append(insertNumberInWord(word))

    return rainbowPasswords

def hashToPassword(hash):
    return Rainbow.reduct(hash)

def passwordToHash(password):
    return getHashFromWord(password)

#entree : hash / sortie : hash suivant
def iterate(hash):
    password = hashToPassword(hash)

    return passwordToHash(password)

def crackHash(password):
    hash = passwordToHash(password)

    return hashToPassword(hash)

def setRainbowTable():
    passwords = getRainbowPasswords()
    for password in passwords:
        hash = passwordToHash(password)
        for i in range(0, REPETITION_M):
            hash = iterate(hash)
        with open('rainbow_table.txt', 'a') as rainbowFile:
            rainbowFile.write("{}|{}\n".format(password, hash))
        
()
