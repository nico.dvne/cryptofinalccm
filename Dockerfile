# RUN DOCKERFILE :
# docker build -t myapp .
# docker run -it --rm -d -v $(pwd):/usr/src/app/ myapp

#Deriving the latest base image
FROM python:latest


#Labels as key value pair
LABEL Maintainer="Samy Mokhtari"


# Any working direcrtory can be chosen as per choice like '/' or '/home' etc
# i have chosen /usr/app/src
WORKDIR /usr/src/app

#to COPY the remote file at working directory in container
COPY *.txt *.py ./ 

# Now the structure looks like this '/usr/app/src/test.py'


#CMD instruction should be used to run the software
#contained by your image, along with any arguments.

CMD [ "python", "./createTable.py"]
