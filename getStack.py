import hashlib
from reduction import Rainbow
import createTable


def getStats(initPassword):
    for i in range(0, createTable.REPETITION_M + 1):
        theHash = createTable.passwordToHash(initPassword)
        print("Hash number " + str(i) + " = " + theHash)
        initPassword = createTable.hashToPassword(theHash)
        print("Password number " + str(i) + " = " + initPassword)


getStats('da3v160enne')
